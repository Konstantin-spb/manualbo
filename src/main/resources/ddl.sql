create schema public
;

comment on schema public is 'standard public schema'
;

create table app_user_profile
(
	id bigserial not null
	constraint app_user_profile_pkey
	primary key,
	type varchar(15) not null
	constraint uk_bj8mv97pise3su1isk73xftev
	unique
	constraint ukbj8mv97pise3su1isk73xftev
	unique
)
;

create table building_objects
(
	id serial not null
	constraint building_objects_pkey
	primary key,
	end_date timestamp,
	object_name varchar(255) not null,
	start_date timestamp
)
;

create table firm
(
	id serial not null
	constraint firm_pkey
	primary key,
	comment varchar(255),
	firm_name varchar(255) not null
)
;

create table addresses
(
	id serial not null
	constraint addresses_pkey
	primary key,
	building_korpus integer,
	building_liter varchar(10),
	building_number integer,
	city varchar(255),
	city_index integer,
	comment varchar(255),
	street varchar(255),
	telephone varchar(30),
	firm_id integer
	constraint fk4br66i9wbwlb57oquldrfxtlu
	references firm
)
;

create table orders
(
	id serial not null
	constraint orders_pkey
	primary key,
	orderdate timestamp,
	order_name varchar(255) not null,
	firm_id integer
	constraint fkgd4sp9l0wesoydxgqfedaasbw
	references firm
)
;

create table building_objects_orders
(
	building_object_id integer not null
	constraint fkp9iehit6pmgbhuk8gxf728be3
	references building_objects,
	order_id integer not null
	constraint fk6u3v8hg12rr042b6piq5d8fi4
	references orders,
	constraint building_objects_orders_pkey
	primary key (building_object_id, order_id)
)
;

create table order_rows
(
	id serial not null
	constraint order_rows_pkey
	primary key,
	count integer,
	item varchar(255) not null,
	price bigint,
	unit varchar(30),
	order_id integer
	constraint fkc4svqi1iddejyys5uq9idehrw
	references orders,
	comments varchar(255)
)
;

create table persistent_logins
(
	series serial not null
	constraint persistent_logins_pkey
	primary key,
	last_used timestamp,
	token varchar(64) not null,
	username varchar(64) not null
)
;

create table status
(
	id bigserial not null
	constraint status_pkey
	primary key,
	status varchar(50) not null
	constraint uk_kxaj0dvn13fwjuimg3y2j0oa2
	unique
	constraint ukkxaj0dvn13fwjuimg3y2j0oa2
	unique
)
;

create table app_user
(
	id bigserial not null
	constraint app_user_pkey
	primary key,
	email varchar(100) not null
	constraint uk1j9d9a06i600gd43uu3km82jw
	unique
	constraint uk_1j9d9a06i600gd43uu3km82jw
	unique,
	first_name varchar(30),
	login_name varchar(30)
	constraint uk_b2p5uxytyfufef7vm8snay5ee
	unique
	constraint ukb2p5uxytyfufef7vm8snay5ee
	unique,
	password varchar(255) not null,
	surname varchar(30),
	status_id bigint not null
	constraint fk2b48lys4xl84end12jffsdfgh
	references status
)
;

create table app_user_app_user_profile
(
	app_user_id bigint not null
	constraint fk6pwevd6am2vpvidopwwenchcn
	references app_user,
	app_user_profile_id bigint not null
	constraint fk126abdsbpli7qyc07uqb5wq0w
	references app_user_profile,
	constraint app_user_app_user_profile_pkey
	primary key (app_user_profile_id, app_user_id)
)
;

create table autorization_history
(
	id bigserial not null
	constraint autorization_history_pkey
	primary key,
	browser_agent varchar(255),
	date_login timestamp not null,
	ip_address varchar(15) not null,
	app_user_id bigint not null
	constraint fk78e0x5emudimi6so6cvs4ud6d
	references app_user
)
;

