INSERT INTO status (id, status) VALUES (1, 'DONE');
INSERT INTO status (id, status) VALUES (2, 'ERROR');
INSERT INTO status (id, status) VALUES (3, 'ENABLE');
INSERT INTO status (id, status) VALUES (4, 'DISABLE');
INSERT INTO status (id, status) VALUES (5, 'IN PROGRESS');

INSERT INTO app_user_profile(type) VALUES ('USER');
INSERT INTO app_user_profile(type) VALUES ('ADMIN');
INSERT INTO app_user_profile(type) VALUES ('DBA');

/* Populate one Admin User which will further create other user for the application using GUI */
INSERT INTO app_user(login_name, password, first_name, surname, email, status_id) VALUES ('konstantin','$2a$10$zYsb6P69Ncobod39iLy.1Oqnu/Ewxc3enou5a5rr0uhZMPO/.xLAy', 'Konstantin','Kuzmin','konstantin-spb@bk.ru', '3');

/* Populate JOIN Table */
INSERT INTO app_user_app_user_profile (app_user_id, app_user_profile_id)  SELECT users.id, app_user_profile.id FROM app_user users, app_user_profile  where users.login_name='konstantin' and app_user_profile.type='ADMIN';


INSERT INTO app_user(login_name, password, first_name, surname, email, status_id) VALUES ('lappo','', 'Lappo','Sergey','12@1.ru', '3');

/* Populate JOIN Table */
INSERT INTO app_user_app_user_profile (app_user_id, app_user_profile_id)  SELECT users.id, app_user_profile.id FROM app_user users, app_user_profile  where users.login_name='lappo' and app_user_profile.type='ADMIN';