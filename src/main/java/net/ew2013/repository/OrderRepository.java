package net.ew2013.repository;

import net.ew2013.model.BuildingObject;
import net.ew2013.model.Order;
import net.ew2013.model.OrderRow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByBuildingObjectsOrderByPayDate(BuildingObject bo);
}