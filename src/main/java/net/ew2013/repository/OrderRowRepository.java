package net.ew2013.repository;

import net.ew2013.model.Order;
import net.ew2013.model.OrderRow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface OrderRowRepository extends JpaRepository<OrderRow, Integer> {

    @Query("select s from OrderRow s where s.ordersByOrderId = :orderId")
    List<OrderRow> findAllRowsByOrderId(@Param("orderId") Order orderId);

//    @Query(value = "SELECT * FROM order_rows WHERE order_id = :orderId;", nativeQuery = true)
//    Set<OrderRow> findAllByOrderId(@Param("orderId") int orderId);
}
