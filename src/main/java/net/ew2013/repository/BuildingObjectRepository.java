package net.ew2013.repository;

import net.ew2013.model.BuildingObject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuildingObjectRepository extends JpaRepository<BuildingObject, Integer> {

    BuildingObject findBuildingObjectByObjectName(String objectName);
    List<BuildingObject> findAllByOrderByObjectName();


}
