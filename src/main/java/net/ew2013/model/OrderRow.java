package net.ew2013.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "order_rows")
public class OrderRow {
    private int id;
    private Integer count;
    private String item;
    private String unit;//Единица измерения
    private Long price;
    private Order ordersByOrderId;
//    private List<OrderrowsPercentage> orderrowsPercentageList;
    private String comments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Basic
    @Column(name = "count", nullable = true)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "item", nullable = false, length = 255)
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Column(name = "unit", length = 30)
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    public Order getOrdersByOrderId() {
        return ordersByOrderId;
    }

    public void setOrdersByOrderId(Order ordersByOrderId) {
        this.ordersByOrderId = ordersByOrderId;
    }

//    @OneToMany(mappedBy = "orderRow")
//    public List<OrderrowsPercentage> getOrderrowsPercentageList() {
//        return orderrowsPercentageList;
//    }

//    public void setOrderrowsPercentageList(List<OrderrowsPercentage> orderrowsPercentageList) {
//        this.orderrowsPercentageList = orderrowsPercentageList;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderRow orderRow = (OrderRow) o;
        return id == orderRow.id &&
                Objects.equals(count, orderRow.count) &&
                Objects.equals(item, orderRow.item) &&
                Objects.equals(unit, orderRow.unit) &&
                Objects.equals(price, orderRow.price) &&
                Objects.equals(ordersByOrderId, orderRow.ordersByOrderId) &&
                Objects.equals(comments, orderRow.comments);
    }

}
