package net.ew2013.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ORDERS")
public class Order {
    private int id;
    private String orderName;
    private Date orderDate;
    private Date payDate;
    private Set<OrderRow> orderRowsById;
    //    private Set<Firm> firms;
    private Set<BuildingObject> buildingObjects;
    private Firm firm;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_name", nullable = false, length = 255)
    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "paydate")
    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "orderdate")
    public Date getDate() {
        return orderDate;
    }

    public void setDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                Objects.equals(orderName, order.orderName) &&
                Objects.equals(orderRowsById, order.orderRowsById) &&
                Objects.equals(firm, order.firm) &&
                Objects.equals(buildingObjects, order.buildingObjects);
    }

    @OneToMany(mappedBy = "ordersByOrderId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Set<OrderRow> getOrderRowsById() {
        return orderRowsById;
    }

    public void setOrderRowsById(Set<OrderRow> orderRows) {
        this.orderRowsById = orderRows;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "firm_id")
    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    @ManyToMany(mappedBy = "orders")
    public Set<BuildingObject> getBuildingObjects() {
        return buildingObjects;
    }

    public void setBuildingObjects(Set<BuildingObject> buildingObjects) {
        this.buildingObjects = buildingObjects;
    }
}
