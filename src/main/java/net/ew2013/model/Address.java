package net.ew2013.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "addresses")
public class Address {
    private int id;
    private Integer cityIndex;
    private String city;
    private String street;
    private Integer buildingNumber;
    private Integer buildingKorpus;
    private String buildingLiter;
    private String telephone;
    private String comment;
//    private Integer firmId;
    private Firm firmByFirmId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "city_index", nullable = true)
    public Integer getCityIndex() {
        return cityIndex;
    }

    public void setCityIndex(Integer cityIndex) {
        this.cityIndex = cityIndex;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 255)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "street", nullable = true, length = 255)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "building_number", nullable = true)
    public Integer getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Integer buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    @Basic
    @Column(name = "building_korpus", nullable = true)
    public Integer getBuildingKorpus() {
        return buildingKorpus;
    }

    public void setBuildingKorpus(Integer buildingKorpus) {
        this.buildingKorpus = buildingKorpus;
    }

    @Basic
    @Column(name = "building_liter", nullable = true, length = 10)
    public String getBuildingLiter() {
        return buildingLiter;
    }

    public void setBuildingLiter(String buildingLiter) {
        this.buildingLiter = buildingLiter;
    }

    @Basic
    @Column(name = "telephone", nullable = true, length = 30)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "comment", nullable = true, length = 255)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    @Basic
//    @Column(name = "firm_id", nullable = true)
//    public Integer getFirmId() {
//        return firmId;
//    }

//    public void setFirmId(Integer firmId) {
//        this.firmId = firmId;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id == address.id &&
                Objects.equals(cityIndex, address.cityIndex) &&
                Objects.equals(city, address.city) &&
                Objects.equals(street, address.street) &&
                Objects.equals(buildingNumber, address.buildingNumber) &&
                Objects.equals(buildingKorpus, address.buildingKorpus) &&
                Objects.equals(buildingLiter, address.buildingLiter) &&
                Objects.equals(telephone, address.telephone) &&
                Objects.equals(comment, address.comment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, cityIndex, city, street, buildingNumber, buildingKorpus, buildingLiter, telephone, comment);
    }

    @ManyToOne
    @JoinColumn(name = "firm_id", referencedColumnName = "id")
    public Firm getFirmByFirmId() {
        return firmByFirmId;
    }

    public void setFirmByFirmId(Firm firmByFirmId) {
        this.firmByFirmId = firmByFirmId;
    }
}
