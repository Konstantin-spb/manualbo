package net.ew2013.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class Firm {
    private int id;
    private String firmName;
    private String coment;
    private Set<Address> addressesById;
//    private Set<Order> orders;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firm_name", nullable = false, length = 255)
    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    @Basic
    @Column(name = "comment", nullable = true, length = 255)
    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Firm firm = (Firm) o;
        return id == firm.id &&
                Objects.equals(firmName, firm.firmName) &&
                Objects.equals(coment, firm.coment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firmName, coment);
    }

    @OneToMany(mappedBy = "firmByFirmId")
    public Set<Address> getAddressesById() {
        return addressesById;
    }

    public void setAddressesById(Set<Address> addressesById) {
        this.addressesById = addressesById;
    }

//    @ManyToMany(mappedBy = "firms")
//    public Set<Order> getOrders() {
//        return orders;
//    }
//
//    public void setOrders(Set<Order> orders) {
//        this.orders = orders;
//    }
}
