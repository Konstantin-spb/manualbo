package net.ew2013.service;


import net.ew2013.model.AppUser;

import java.util.List;

public interface UsersService {
	
	AppUser findOne(int id);

	AppUser findByName(String name);
	
	void saveOrUpdate(AppUser user);
	
	List<AppUser> findAll();
	
	boolean isUsersSSOUnique(Long id, String sso);

	String getPrincipal();
}