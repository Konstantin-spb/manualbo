package net.ew2013.service;

import net.ew2013.model.AppUser;
import net.ew2013.repository.AppUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userService")
@Transactional
public class UserServiceImpl implements UsersService {

	@Autowired
    AppUsersRepository appUsersRepository;

	@Autowired
    private PasswordEncoder passwordEncoder;
	
	public AppUser findOne(int id) {
		return appUsersRepository.findOne(id);
	}

	public AppUser findByName(String login) {
		AppUser user = appUsersRepository.findByName(login);
		return user;
	}

	public void saveOrUpdate(AppUser user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		appUsersRepository.save(user);
	}

	public List<AppUser> findAll() {
		return appUsersRepository.findAll();
	}

	public boolean isUsersSSOUnique(Long id, String sso) {
		AppUser user = findByName(sso);
		return ( user == null || ((id != null) && (user.getId() == id)));
	}

	@Override
	public String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
}
