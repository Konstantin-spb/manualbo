package net.ew2013.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Component
public class NativeSqlQueries {

    @Autowired
    EntityManager entityManager;

    public List getAllBo(Date startDate, Date endDate) {
        List anwser = entityManager.createNativeQuery("SELECT OB.OBJECT_NAME, OB.START_DATE,OB.END_DATE, SUM(OR2.PRICE), OB.ID FROM BUILDING_OBJECTS OB " +
                "  LEFT JOIN BUILDING_OBJECTS_ORDERS ORDER2 ON OB.ID = ORDER2.BUILDING_OBJECT_ID " +
                "  LEFT JOIN ORDERS O ON ORDER2.ORDER_ID = O.ID " +
                "  LEFT JOIN ORDER_ROWS OR2 ON O.ID = OR2.ORDER_ID " +
                "WHERE OB.start_date >= :startDate AND OB.start_date <= :endDate " +
                "GROUP BY OB.OBJECT_NAME, OB.START_DATE,OB.END_DATE,OB.ID ORDER BY OB.OBJECT_NAME;")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
//        List anwser = entityManager.createNativeQuery("SELECT OB.OBJECT_NAME, OB.START_DATE,OB.END_DATE, SUM(OR2.PRICE), OB.ID FROM BUILDING_OBJECTS OB " +
//                "  LEFT JOIN BUILDING_OBJECTS_ORDERS ORDER2 ON OB.ID = ORDER2.BUILDING_OBJECT_ID " +
//                "  LEFT JOIN ORDERS O ON ORDER2.ORDER_ID = O.ID " +
//                "  LEFT JOIN ORDER_ROWS OR2 ON O.ID = OR2.ORDER_ID " +
//                "GROUP BY OB.OBJECT_NAME, OB.START_DATE,OB.END_DATE,OB.ID ORDER BY OB.OBJECT_NAME;").getResultList();
        System.out.println();
        return anwser;
    }

    public List getAllOrdersByDate(Date startDate, Date endDate) {

        return entityManager.createNativeQuery("" +
                "SELECT * FROM building_objects bo LEFT JOIN building_objects_orders boo ON bo.id = boo.building_object_id\n" +
                "LEFT JOIN orders o ON boo.order_id = o.id\n" +
                "WHERE o.paydate >= :startDate AND o.paydate <= :endDate ;")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }

//    public List getAllOrdersByDate(Date startDate, Date endDate) {
//        List anwser = entityManager.createNativeQuery("" +
//                "SELECT * FROM building_objects bo LEFT JOIN building_objects_orders boo ON bo.id = boo.building_object_id\n" +
//                "LEFT JOIN orders o ON boo.order_id = o.id\n" +
//                "WHERE o.orderdate >= :startDate AND o.orderdate <= :endDate;")
//                .setParameter("startDate", startDate)
//                .setParameter("endDate", endDate)
//                .getResultList();
//
//        return anwser;
//    }
}
