package net.ew2013.controller;

import net.ew2013.model.Address;
import net.ew2013.model.Firm;
import net.ew2013.repository.AddressRepository;
import net.ew2013.repository.FirmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import webobject.FirmAndAddress;

@Controller
@Scope(value = "prototype")
@RequestMapping("/firms")
public class FirmController {
  @Autowired
  FirmRepository firmRepository;

  @Autowired
  AddressRepository addressRepository;

  @RequestMapping(value = "/{currentuser}/new-firm/create/pls", method = RequestMethod.GET)
  public String createNewFirm(@RequestParam("boId") String buildingObjectId, Model model) {
    //TODO создать HTML страницу для создания новой фирмы и адреса. Спасибо.
    model.addAttribute("firmAndAddress", new FirmAndAddress());
    model.addAttribute("buildingObjectId", buildingObjectId);
    return "user/firm";
  }

//  @RequestMapping(value = "/{currentuser}/new-firm/create/pls", method = RequestMethod.POST)
//  public String saveFirm(@ModelAttribute("firmAndAddress") FirmAndAddress firmAndAddress) {
//
//    Firm firm = new Firm();
//    Address address = new Address();
//
//    firm.setFirmName(firmAndAddress.getFirmName());
//    firm.setComent(firmAndAddress.getCommentForFirm());
//
//    address.setCityIndex(Integer.parseInt(firmAndAddress.getCityIndex()));
//    address.setCity(firmAndAddress.getCityName());
//    address.setStreet(firmAndAddress.getStreet());
//    //Проверка на null не нужна, так как в классе(kotlin) определено значение по умолчанию
//    if (!firmAndAddress.getBuildingNumber().isEmpty()) {
//      address.setBuildingNumber(Integer.parseInt(firmAndAddress.getBuildingNumber()));
//    }
//    if (!firmAndAddress.getBuildingKorpus().isEmpty()) {
//      address.setBuildingKorpus(Integer.parseInt(firmAndAddress.getBuildingKorpus()));
//    }
//    if (!firmAndAddress.getBuildingLitera().isEmpty()) {
//      address.setBuildingLiter(firmAndAddress.getBuildingLitera());
//    }
//    if (!firmAndAddress.getBuildingNumber().isEmpty()) {
//      address.setTelephone(firmAndAddress.getTelephone());
//    }
//    address.setComment(firmAndAddress.getCommentForAddress());
//
//    firm = firmRepository.save(firm);
//    address.setFirmByFirmId(firm);
//    address = addressRepository.save(address);
//
//    System.out.println();
////        return "redirect:/users/" + getPrincipal();
//    return "redirect:/users/" + getPrincipal() + "/bo-" + firmAndAddress.getBoId() + "/order/new";//http://localhost:8080/manualBO-1.0/users/konstantin/bo-3/order/new
//  }
}
