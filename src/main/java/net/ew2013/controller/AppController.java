package net.ew2013.controller;

import net.ew2013.model.AppUser;
import net.ew2013.model.AppUserProfile;
import net.ew2013.model.BuildingObject;
import net.ew2013.repository.AppUserProfileRepository;
import net.ew2013.repository.AppUsersRepository;
import net.ew2013.repository.BuildingObjectRepository;
import net.ew2013.repository.StatusRepository;
import net.ew2013.service.UserProfileService;
import net.ew2013.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

	@Autowired
	UsersService userService;
	@Autowired
	UserProfileService userProfileService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	@Autowired
	AppUserProfileRepository appUserProfileRepository;
	@Autowired
	AppUsersRepository appUsersRepository;
	@Autowired
	StatusRepository statusRepository;

	@RequestMapping(value = "/")
	public String startHere() {
		return "redirect:/users/" + getPrincipal();
	}

	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		AppUser appUser = new AppUser();
		model.addAttribute("appUser", appUser);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipal());
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
	@Transactional
	public String saveUser(@Valid @ModelAttribute("appUser") AppUser appUser, BindingResult result,
						   ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}

		/*
		 * Preferred way to achieve uniqueness of field [sso] should be implementing custom @Unique annotation 
		 * and applying it on field [sso] of Model class [User].
		 * 
		 * Below mentioned peace of code [if block] is to demonstrate that you can fill custom errors outside the validation
		 * framework as well while still using internationalized messages.
		 * 
		 */
		List<AppUser> appUsers = appUsersRepository.findAllByEmail(appUser.getEmail());
		if(!userService.isUsersSSOUnique(appUser.getId(), appUser.getLoginName())){
			FieldError ssoError = new FieldError("user","loginName",messageSource.getMessage("non.unique.login", new String[]{appUser.getLoginName()}, Locale.getDefault()));
		    result.addError(ssoError);
			return "registration";

		}  else if ( appUsers != null && appUsers.size() > 0) {
			FieldError emailError = new FieldError("user","email",messageSource.getMessage("non.unique.email", new String[]{appUser.getEmail()}, Locale.getDefault()));
			result.addError(emailError);
			return "registration";
		}

		AppUserProfile appUserProfile = appUserProfileRepository.getOne(1L);
		Set<AppUserProfile> appUserProfileSet = new HashSet<>();
		appUserProfileSet.add(appUserProfile);//Set user only profile by default
		appUser.setAppUserProfiles(appUserProfileSet);
		appUser.setStatus(statusRepository.findOne(4L));//Set desable by default
		userService.saveOrUpdate(appUser);

		model.addAttribute("success", "User " + appUser.getFirstName() + " "+ appUser.getSurname() + " registered successfully");
		model.addAttribute("loggedinuser", getPrincipal());
		//return "success";
		return "registrationsuccess";
	}


	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/edit-user-{login}" }, method = RequestMethod.GET)
	@Transactional
	public String editUser(@PathVariable String login, ModelMap model) {
		AppUser user = userService.findByName(login);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipal());
		return "registration";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/edit-user-{login}" }, method = RequestMethod.POST)
	public String updateUser(@Valid AppUser user, BindingResult result,
							 ModelMap model, @PathVariable String login) {

		if (result.hasErrors()) {
			return "registration";
		}
		/*//Uncomment below 'if block' if you WANT TO ALLOW UPDATING SSO_ID in UI which is a unique key to a User.
		if(!userService.isUserSSOUnique(user.getId(), user.getSsoId())){
			FieldError ssoError =new FieldError("user","login",messageSource.getMessage("non.unique.login", new String[]{user.getSsoId()}, Locale.getDefault()));
		    result.addError(ssoError);
			return "registration";
		}*/
		userService.saveOrUpdate(user);
		model.addAttribute("success", "User " + user.getFirstName() + " "+ user.getSurname() + " updated successfully");
		model.addAttribute("loggedinuser", getPrincipal());
//		return "registrationsuccess";
		return "login";
	}

	/**
	 * This method will delete an user by it's SSOID value.
	 */
	@RequestMapping(value = { "/delete-user-{login}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String login) {
//		userService.deleteUserBySSO(login);
		AppUser appUser = appUsersRepository.findByName(login);
		appUsersRepository.delete(appUser);
		return "redirect:/admin/list";
	}

	/**
	 * This method will provide AppUserProfile list to views
	 */
	@ModelAttribute("roles")
	public List<AppUserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}
	
	/**
	 * This method handles Access-Denied redirect.
	 */
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", getPrincipal());
		return "accessDenied";
	}

	/**
	 * This method handles login GET requests.
	 * If users is already logged-in and tries to goto login page again, will be redirected to list page.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		if (isCurrentAuthenticationAnonymous()) {
			return "login";
	    } else {
	    	return "redirect:/users/" + getPrincipal();
	    }
	}

	/**
	 * This method handles logout requests.
	 * Toggle the handlers if you are RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){    
			persistentTokenBasedRememberMeServices.logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}

	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
	
	/**
	 * This method returns true if users is already authenticated [logged-in], else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    return authenticationTrustResolver.isAnonymous(authentication);
	}


}