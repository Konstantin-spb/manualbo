package net.ew2013.controller;

import com.google.gson.Gson;
import net.ew2013.model.*;
import net.ew2013.repository.*;
import net.ew2013.service.UsersService;
import net.ew2013.test.NativeSqlQueries;
import net.ew2013.webobjects.BoWeb;
import net.ew2013.webobjects.OrderRowWeb;
import net.ew2013.webobjects.OrderWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.SqlServiceKt;
import webobject.BuildingObjectProxy;
import webobject.FirmAndAddress;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

//import java.sql.Date;

/**
 * Created by konstantin on 04.08.16.
 */
@Controller
@Scope(value = "prototype")
@RequestMapping("/users")
public class UserController {

  @Autowired
  UsersService usersService;
  @Autowired
  AppUsersRepository appUsersRepository;
  @Autowired
  AutorizationHistoryRepository autorizationHistoryRepository;
  @Autowired
  ThreadPoolTaskExecutor threadPoolTaskExecutor;
  @Autowired
  BuildingObjectRepository buildingObjectRepository;
  @Autowired
  NativeSqlQueries nativeSqlQueries;
  @Autowired
  AddressRepository addressRepository;
  @Autowired
  OrderRowRepository orderRowRepository;
  @Autowired
  FirmRepository firmRepository;
  @Autowired
  OrderRepository orderRepository;
  @Autowired
  MessageSource messageSource;

  @RequestMapping(value = "/{currentuser}")
  public String userStartPage(@PathVariable String currentuser, Model model) {

    AppUser appUser = usersService.findByName(usersService.getPrincipal());

    if (currentuser.equals(appUser.getLoginName())) {
      //some logic here
      DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
      List<BuildingObject> boList = buildingObjectRepository.findAll();

      List bo = null;
      try {
//        boList =  SqlServiceKt.filterOrderByDateRange(boList, format.parse("2019-01-01"), format.parse("2019-12-01"));
        bo = nativeSqlQueries.getAllBo(format.parse("2000-01-01"), new java.util.Date());
      } catch (ParseException e) {
        e.printStackTrace();
      }
      model.addAttribute("appUser", appUser);
      model.addAttribute("bo", bo);
      return "/user/bolist";
    }
    return "redirect:/";
  }

  @RequestMapping(value = "/{currentuser}/filter-bo-by-date", method = RequestMethod.POST)
  // @Async
  public ModelAndView getAllBoByStartdate(@RequestParam("startDate") String startDate,
                                          @RequestParam("endDate") String endDate) {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    ModelMap model = new ModelMap();
    AppUser appUser = usersService.findByName(usersService.getPrincipal());

    List bo = null;
    try {
//            generalStatRepository.getStatsByDate(format.parse(startDate), format.parse(endDate), user.getId())).replaceAll("null", "0").replaceAll("\\n", ""));
      bo = nativeSqlQueries.getAllBo(format.parse(startDate), format.parse(endDate));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    model.addAttribute("appUser", appUser);
    model.addAttribute("bo", bo);
    return new ModelAndView("/user/bolist", model);
  }

  @RequestMapping(value = "/{currentuser}/bo-{boId}/order/new", method = RequestMethod.GET)
  public String addNewOrder(@PathVariable String boId, Model model) {

    List<Firm> firmList = firmRepository.findAll();
    List<BuildingObject> buildingObjectList = buildingObjectRepository.findAll();
    OrderRowWeb orderRowWeb = new OrderRowWeb();
    BuildingObject bo = buildingObjectRepository.findOne(Integer.parseInt(boId));

    model.addAttribute("orderWeb", new OrderWeb());
    model.addAttribute("firmList", firmList);
    model.addAttribute("bolist", buildingObjectList);
    model.addAttribute("loggedinuser", getPrincipal());
    model.addAttribute("bo", bo);
    model.addAttribute("orderRowWeb", orderRowWeb);
    System.out.println("Создали новый ордер");
    return "user/order";
  }

  @RequestMapping(value = "/{currentuser}/bo-{boId}/order/new", method = RequestMethod.POST)
  public String saveNewOrderRow(@ModelAttribute("orderWeb") OrderWeb orderWeb, @PathVariable String boId) {

    Gson gson = new Gson();
    String[][] strings = gson.fromJson(orderWeb.getJsonArray(), String[][].class);
    System.out.println(orderWeb.getJsonArray());
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(boId));

    if (strings.length < 1) {
      return "user/order";
    }
    //TODO нужно сохранить ордер
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    Order order = new Order();
//        order.setPayDate(new Date(new java.util.Date().getTime()));
    order.setOrderName(orderWeb.getOrderName());
    try {
      if (!orderWeb.getOrderDate().isEmpty()) {
        order.setDate(format.parse(orderWeb.getOrderDate()));
      }
      if (!orderWeb.getPayDate().isEmpty()) {
        order.setPayDate(format.parse(orderWeb.getPayDate()));
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    order = orderRepository.save(order);
    for (String[] ar : strings) {
      OrderRow orderRow = new OrderRow();
//            OrderrowsPercentage orderrowsPercentage = new OrderrowsPercentage();
      orderRow.setItem(ar[0]);
      if (ar[1] != null && !ar[1].isEmpty()) {
        orderRow.setCount(Integer.parseInt(ar[1]));
      }
      if (ar[2] != null && !ar[2].isEmpty()) {
        orderRow.setUnit(ar[2]);
      }
      if (ar[3] != null && !ar[3].isEmpty()) {
        orderRow.setPrice(Long.parseLong(ar[3].replaceAll("\\D", "")));
      }
      if (ar[4] != null && !ar[4].isEmpty()) {
        orderRow.setComments(ar[4]);
      }

      orderRow.setOrdersByOrderId(order);
      orderRowRepository.save(orderRow);
    }
    Firm firm = firmRepository.findOne(Integer.parseInt(orderWeb.getFirmId()));
    order.setFirm(firm);
    order = orderRepository.save(order);

    Set<Order> orderSet = new HashSet<>(orderRepository.findAllByBuildingObjectsOrderByPayDate(buildingObject));
    orderSet.add(order);
    buildingObject.setOrders(orderSet);
    buildingObjectRepository.save(buildingObject);

    return "redirect:/users/" + getPrincipal() + "/orders/show/bo-" + boId;
  }

  @RequestMapping(value = "/{currentuser}/bo-{boId}/order-{orderId}/edit", method = RequestMethod.GET)
  public String editOrder(@PathVariable("boId") String boId, @PathVariable("orderId") String orderId, Model model) {
    DateFormat dateToString = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    Order order = orderRepository.findOne(Integer.parseInt(orderId));
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(boId));

    OrderWeb orderWeb = new OrderWeb();
    orderWeb.setOrderName(order.getOrderName());
    if (order.getDate() != null) {
      orderWeb.setOrderDate(dateToString.format(order.getDate()));
    }
    if (order.getPayDate() != null) {
      orderWeb.setPayDate(dateToString.format(order.getPayDate()));
    }
    if (order.getFirm() != null) {
      orderWeb.setFirmId(String.valueOf(order.getFirm().getId()));
    }
    model.addAttribute("loggedinuser", getPrincipal());
    model.addAttribute("orderWeb", orderWeb);
    model.addAttribute("orderRowWeb", new OrderRowWeb());
    model.addAttribute("order", order);
    model.addAttribute("bo", buildingObject);
    return "user/order";
  }

  @RequestMapping(value = "/{currentuser}/bo-{boId}/order-{orderId}/edit", method = RequestMethod.POST)
  public String saveEditOrderRow(@ModelAttribute("orderWeb") OrderWeb orderWeb, @PathVariable String boId, @PathVariable String orderId) {

    Order order = orderRepository.findOne(Integer.parseInt(orderId));
    Gson gson = new Gson();
    String[][] strings = gson.fromJson(orderWeb.getJsonArray(), String[][].class);
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(boId));

    System.out.println(orderWeb.getJsonArray());

    if (strings.length < 1) {
      return "user/order";
    }
    //TODO нужно сохранить ордер
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    order.setOrderName(orderWeb.getOrderName());
    try {
      if (!orderWeb.getOrderDate().isEmpty()) {
        order.setDate(format.parse(orderWeb.getOrderDate()));
      }
      if (!orderWeb.getPayDate().isEmpty()) {
        order.setPayDate(format.parse(orderWeb.getPayDate()));
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    order = orderRepository.save(order);

    List<OrderRow> orderRowSet = orderRowRepository.findAllRowsByOrderId(order);

//        for (OrderRow row :
//                orderRowSet) {
//            List<OrderrowsPercentage> orderrowsPercentageList  = orderrowsPercentageRepository.findByOrderRow(row);
//            orderrowsPercentageRepository.delete(orderrowsPercentageList);
//        }

    orderRowRepository.delete(orderRowSet);
    orderRowSet.clear();

    for (String[] ar : strings) {
      OrderRow orderRow = new OrderRow();
//            OrderrowsPercentage orderrowsPercentage = new OrderrowsPercentage();
      orderRow.setItem(ar[0]);
      if (ar[1] != null && !ar[1].isEmpty()) {
        orderRow.setCount(Integer.parseInt(ar[1]));
      }
      if (ar[2] != null && !ar[2].isEmpty()) {
        orderRow.setUnit(ar[2]);
      }
      if (ar[3] != null && !ar[3].isEmpty()) {
        orderRow.setPrice(Long.parseLong(ar[3].replaceAll("\\D", "")));
      }
      if (ar[4] != null && !ar[4].isEmpty()) {
        orderRow.setUnit(ar[4]);
      }
      orderRow.setOrdersByOrderId(order);
      orderRowSet.add(orderRowRepository.save(orderRow));
    }

    Firm firm = firmRepository.findOne(Integer.parseInt(orderWeb.getFirmId()));
    order.setFirm(firm);
    orderRowSet = orderRowRepository.save(orderRowSet);
    order.setOrderRowsById(new HashSet<>(orderRowSet));
    order = orderRepository.save(order);

    orderRowRepository.findAllRowsByOrderId(order);

    Set<Order> orderSet = new HashSet<Order>(orderRepository.findAllByBuildingObjectsOrderByPayDate(buildingObject));
    orderSet.add(order);
    buildingObject.setOrders(orderSet);
    buildingObjectRepository.save(buildingObject);

    return "redirect:/users/" + getPrincipal() + "/orders/show/bo-" + boId;
  }

  @RequestMapping(value = "/{currentuser}/bo-{boId}/order-{orderId}/moveto", method = RequestMethod.GET)
  public String moveOrderTo(@PathVariable("boId") String boId, @PathVariable("orderId") String orderId, Model model) {
    model.addAttribute("order", orderRepository.findOne(Integer.parseInt(orderId)));
    model.addAttribute("oldBoId", boId);
//        model.addAttribute("boList", buildingObjectRepository.findAll());
    model.addAttribute("boList", buildingObjectRepository.findAllByOrderByObjectName());
    model.addAttribute("loggedinuser", getPrincipal());
    return "user/move-order-to";
  }

  @Transactional
  @RequestMapping(value = "/{currentuser}/bo-{boId}/order-{orderId}/moveto", method = RequestMethod.POST)
  public String moveOrderSaveToBase(@RequestParam("boId") String newBoId, @PathVariable("boId") String oldBoId, @PathVariable("orderId") String orderId) {
    Order order = orderRepository.findOne(Integer.parseInt(orderId));
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(oldBoId));

    order.getBuildingObjects().remove(buildingObject);
    orderRepository.save(order);

    buildingObject.getOrders().remove(order);
    buildingObjectRepository.save(buildingObject);

    buildingObject = buildingObjectRepository.findOne(Integer.parseInt(newBoId));

    buildingObject.getOrders().add(order);
    buildingObjectRepository.save(buildingObject);

    return "redirect:/users/" + getPrincipal() + "/orders/show/bo-" + oldBoId;
  }

  @Transactional
  @RequestMapping(value = "/{currentuser}/bo-{boId}/order-{orderId}/delete", method = RequestMethod.GET)
  public String deleteOrder(@PathVariable("boId") String boId, @PathVariable("orderId") String orderId) {
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(boId));
    Order order = orderRepository.findOne(Integer.parseInt(orderId));
    Set<OrderRow> orderRowSet = order.getOrderRowsById();

    Set<Order> orderSet = buildingObject.getOrders();
    orderSet.remove(order);
    orderSet = new HashSet<>(orderRepository.save(orderSet));

    buildingObject.setOrders(orderSet);
    buildingObjectRepository.save(buildingObject);
    orderRowRepository.delete(orderRowSet);
//        orderRepository.delete(order);
    System.out.println("Счёт номер " + orderId);
    return "redirect:/users/" + getPrincipal() + "/orders/show/bo-" + boId;
  }

  @Transactional
  @RequestMapping(value = "/{currentuser}/bo-{boId}/delete", method = RequestMethod.GET)
  public String deleteBuildingObject(@PathVariable("boId") String boId) {
    BuildingObject buildingObject = buildingObjectRepository.findOne(Integer.parseInt(boId));

    for (Order order : buildingObject.getOrders()) {
      Set<OrderRow> orderRowSet = order.getOrderRowsById();
      orderRowRepository.delete(orderRowSet);
    }
    orderRepository.delete(buildingObject.getOrders());
    buildingObjectRepository.delete(buildingObject);
    System.out.println("Строительный объект " + "-=" + buildingObject.getObjectName() + "=-" + " удалён, восстановлению не подлежит.");
    return "redirect:/";
  }

  @RequestMapping(value = "/{currentuser}/new-firm/create/", method = RequestMethod.GET)
  public String createNewFirm(@RequestParam("boId") String buildingObjectId, Model model) {
    //TODO создать HTML страницу для создания новой фирмы и адреса. Спасибо.
    model.addAttribute("firmAndAddress", new FirmAndAddress());
    model.addAttribute("buildingObjectId", buildingObjectId);
    return "user/firm";
  }

  @RequestMapping(value = "/{currentuser}/new-firm/create/", method = RequestMethod.POST)
  public String saveFirm(@ModelAttribute("firmAndAddress") FirmAndAddress firmAndAddress) {

    Firm firm = new Firm();
    Address address = new Address();

    firm.setFirmName(firmAndAddress.getFirmName());
    firm.setComent(firmAndAddress.getCommentForFirm());

    address.setCityIndex(Integer.parseInt(firmAndAddress.getCityIndex()));
    address.setCity(firmAndAddress.getCityName());
    address.setStreet(firmAndAddress.getStreet());
    //Проверка на null не нужна, так как в классе(kotlin) определено значение по умолчанию
    if (!firmAndAddress.getBuildingNumber().isEmpty()) {
      address.setBuildingNumber(Integer.parseInt(firmAndAddress.getBuildingNumber()));
    }
    if (!firmAndAddress.getBuildingKorpus().isEmpty()) {
      address.setBuildingKorpus(Integer.parseInt(firmAndAddress.getBuildingKorpus()));
    }
    if (!firmAndAddress.getBuildingLitera().isEmpty()) {
      address.setBuildingLiter(firmAndAddress.getBuildingLitera());
    }
    if (!firmAndAddress.getBuildingNumber().isEmpty()) {
      address.setTelephone(firmAndAddress.getTelephone());
    }
    address.setComment(firmAndAddress.getCommentForAddress());

    firm = firmRepository.save(firm);
    address.setFirmByFirmId(firm);
    address = addressRepository.save(address);

    System.out.println();
//        return "redirect:/users/" + getPrincipal();
    return "redirect:/users/" + getPrincipal() + "/bo-" + firmAndAddress.getBoId() + "/order/new";//http://localhost:8080/manualBO-1.0/users/konstantin/bo-3/order/new
  }

  @RequestMapping(value = "/{currentuser}/orders/show/bo-{boId}", method = RequestMethod.GET)
  public String showAllOrders(@PathVariable int boId, Model model) {
    BuildingObject buildingObject = buildingObjectRepository.getOne(boId);
    List<Order> orderList = orderRepository.findAllByBuildingObjectsOrderByPayDate(buildingObject);

    model.addAttribute("bo", buildingObject);
    model.addAttribute("loggedinuser", getPrincipal());
    if (orderList != null && orderList.size() > 0) {
      Set<OrderRow> orderRowList = orderList.get(0).getOrderRowsById();
      model.addAttribute("orders", orderList);
    } else {

    }
    return "/user/orderslist";
  }

  @RequestMapping(value = {"/{currentuser}/new-bo/add"}, method = RequestMethod.GET)
  public String addNewBo(ModelMap model) {
//        AppUser appUser = new AppUser();
    BoWeb boWeb = new BoWeb();
    model.addAttribute("boWeb", boWeb);
    model.addAttribute("edit", false);
    model.addAttribute("loggedinuser", getPrincipal());
    return "/user/bo";
  }

  @RequestMapping(value = {"/{currentuser}/new-bo/add"}, method = RequestMethod.POST)
  @Transactional
  public String saveBo(@Valid @ModelAttribute("boWeb") BoWeb boWeb, BindingResult result,
                       ModelMap model) {

    if (result.hasErrors()) {
      return "/user/bo";
    }
//TODO разобраться с передачей даты с web в объект
    BuildingObject buildingObject = buildingObjectRepository.findBuildingObjectByObjectName(boWeb.getObjectName());
    if (buildingObject != null) {
      FieldError boError = new FieldError("boWeb", "objectName", "Объект с таким названием уже есть");
      result.addError(boError);
      return "/user/bo";
    } else {
      try {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        buildingObject = new BuildingObject();
        buildingObject.setObjectName(boWeb.getObjectName());
        if (boWeb.getStartDate() != null && !boWeb.getStartDate().isEmpty()) {
          buildingObject.setStartDate(format.parse(boWeb.getStartDate()));
        }
        if (boWeb.getEndDate() != null && !boWeb.getEndDate().isEmpty()) {
          buildingObject.setEndDate(format.parse(boWeb.getEndDate()));
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }

    buildingObjectRepository.save(buildingObject);

    return "redirect:/users/" + getPrincipal();
  }

  @RequestMapping(value = {"/{currentuser}/filter-orders-by-date"}, method = RequestMethod.POST)
  public ModelAndView getAllOrdersByDate(@RequestParam("startDate") String startDate,
                                         @RequestParam("endDate") String endDate) {

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    ModelMap model = new ModelMap();
    AppUser appUser = usersService.findByName(usersService.getPrincipal());

    List<BuildingObject> boList = buildingObjectRepository.findAll();
    List<BuildingObjectProxy> webBoList = new ArrayList<>();
    try {
      Date sDate = format.parse(startDate);
      Date eDate = format.parse(endDate);
      webBoList =  SqlServiceKt.filterOrderByDateRange(boList, sDate, eDate);
//      bo = nativeSqlQueries.getAllOrdersByDate(sDate, eDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    model.addAttribute("appUser", appUser);
    model.addAttribute("webBoList", webBoList);
    return new ModelAndView("/user/filtered-bolist", model);
  }

  private String getPrincipal(){
    String userName = null;
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    if (principal instanceof UserDetails) {
      userName = ((UserDetails)principal).getUsername();
    } else {
      userName = principal.toString();
    }
    return userName;
  }

}