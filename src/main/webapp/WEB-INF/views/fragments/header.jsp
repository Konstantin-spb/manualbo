<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
<title>Петербург Электро</title>
	<spring:url value="/static/css/bootstrap.min.css" var="bootstrapCss" />
	<spring:url value="/static/css/spoiler.css" var="spoilerCss" />
	<link href="${bootstrapCss}" rel="stylesheet" />
	<link href="${spoilerCss}" rel="stylesheet" />

	<style type="text/css">
		.spoiler_body {display:none; cursor:pointer;}
	</style>
	<script>

        function addRowToOrder(item, count, unit, price, comments, orderRowId) {
            var tbody = document.getElementById("mainTableBody");
            var row = document.createElement("TR");
            var deleteButton = document.createElement("input");
            deleteButton.type = "button";
            deleteButton.onclick = deleteRow();
            deleteButton.value = "delete";
            row.appendChild(createTdElement().appendChild(deleteButton));

            var rowId = document.createElement("input");
            rowId.type = "hidden";
            rowId.className = "rowId";
            rowId.value = orderRowId;

            row.appendChild(createTdElement(21, item, "item"));
            row.appendChild(createTdElement(4, count, "count"));
            row.appendChild(createTdElement(3, unit, "unit"));
            row.appendChild(createTdElement(10, price, "price"));
            row.appendChild(createTdElement(3, comments, "comments"));
            row.appendChild(rowId);

            tbody.appendChild(row);
            clearInputFields();
        }

        function clearInputFields() {
            document.getElementById("item_id").value = "";
            document.getElementById("count_id").value = "";
            document.getElementById("unit_id").value = "";
            document.getElementById("price_id").value = "";
            document.getElementById("comments").value = "";
        }

        function createTdElement(shirina, param, className) {
            var td = document.createElement('td');
            td.classList.add('green_buts');
            td.innerHTML = '<textarea class="' + className + '" cols="' + shirina + '" valua="' + param + '">' + param + '</textarea>';
            return td;
        }

        function deleteRow() {
            Array.from(document.querySelectorAll('#mainTableBody input[type="button"]')).forEach(function (e) {
                e.addEventListener('click', function () {
                    var a = this.closest('tr');
                    a.parentElement.removeChild(a);
                });
            });
        }

        function tableValueToJson(tbodyElem) {
            var tbody = document.getElementById(tbodyElem);
            var trArray = tbody.rows;
            var arrayForJson = [];

            for (var i = 0; i < trArray.length; i++) {
                arrayForJson[i] = [];
                for (var j = 0; j < trArray[i].cells.length; j++) {
                    arrayForJson[i][j] = trArray[i].cells[j].firstElementChild.innerHTML;
                }
            }
            var str = JSON.stringify(arrayForJson);

            document.querySelector('#springJson').value = str;
        }

        //Форматирование чисел для удобочитаемости
        function formatNumberStr(str) {
            str = str.replace(/(\.(.*))/g, '');
            var arr = str.split('');
            var str_temp = '';
            if (str.length > 3) {
                for (var i = arr.length - 1, j = 1; i >= 0; i--, j++) {
                    str_temp = arr[i] + str_temp;
                    if (j % 3 == 0) {
                        str_temp = ' ' + str_temp;
                    }
                }
                return str_temp;
            } else {
                return str;
            }
            console.log(str);
        }
	</script>

</head>
