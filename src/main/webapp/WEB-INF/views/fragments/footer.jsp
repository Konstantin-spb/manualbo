<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="container">
    <hr>
    <footer>
        <p>&copy; Konstantin 2017 - 2018</p>
    </footer>
</div>

<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.spoiler_links').click(function(){
            $(this).parent().children('div.spoiler_body').toggle('normal');
            return false;
        });
    });
</script>

<script>
    var elems = document.getElementsByClassName("price");
    var sum = +0;
    var formatter = new Intl.NumberFormat('ru');

    for (var i = 0; i < elems.length; i++) {
        console.log(elems[i].innerHTML.trim());
        // formatNumberStr(elems[i].innerHTML);
        var el = +elems[i].innerHTML.trim() / 100;
        elems[i].innerHTML = formatter.format(el); // 1 000 000
        sum = sum + el;
    }

    document.getElementById('sum').innerHTML = formatter.format(sum);

</script>


