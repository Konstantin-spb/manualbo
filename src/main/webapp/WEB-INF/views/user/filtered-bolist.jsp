<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<spring:url value="/" var="urlHome"/>
<spring:url value='/users/${appUser.loginName}/new-bo/add' var="urlAddBo"/>
<jsp:useBean id="now" class="java.util.Date" scope="page"/>
 
<%--<fmt:formatDate type="time" value="${now}" pattern="yyyy-MM-dd"/>--%>


<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">Петербург Электро</a>
        </div>

        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/logout" />">Выйти из учетки</a>
        </div>
        </span> <span><a
            href="<c:url value="/logout" />">Logout</a></span>

        <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="${urlAddBo}">Добавить новый объект</a></li>
            </ul>
        </div>
    </div>
</nav>

<body>
<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <table>
        <tr>
            <td>
                <h1>Список объектов</h1>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                Показать объекты<br>только с оплаченными<br>счетами за период:
            </td>
            <td >
                <%--<form action="<c:url value='/users/${appUser.loginName}/filter-bo-by-date'/>" method="POST">--%>
                <form action="<c:url value='/users/${appUser.loginName}/filter-orders-by-date'/>" method="POST">
                    <input type="date" name="startDate" value="2010-01-01" min="2000-01-01">
                    <input type="date" name="endDate"
                           value="<fmt:formatDate type="time" value="${now}" pattern="yyyy-MM-dd"/>" min="2000-01-01">
                    <button type="submit" class="btn btn-secondary">Отфильтровать</button>
                </form>
            </td>
        </tr>
    </table>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Название объекта</th>
            <th>Дата заключения договора</th>
            <th>Дата окончания работ</th>
            <th>Сумма(руб.)</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <%--Это конструкция if...else--%>
            <c:when test="${webBoList.size() > 0}">
                <c:forEach items="${webBoList}" var="webBo">
                    <tr class="line_tr">
                        <td>
                            <a href="<c:url value='/users/${appUser.loginName}/orders/show/bo-${webBo.id}'/>">${webBo.name}</a>
                        </td>
                        <td>
                            <c:set var="dateVar" value="${webBo.startDate}"/>
                            <fmt:formatDate value="${dateVar}" pattern="dd MMM yyyy"/>
                        </td>
                        <td>
                            <c:set var="dateVar" value="${webBo.endDate}"/>
                            <fmt:formatDate value="${dateVar}" pattern="dd MMM yyyy"/>
                        </td>
                        <td class="price">
                                ${webBo.sum}
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                Нет ордеров
            </c:otherwise>
        </c:choose>
        <tr>
            <td><h3>ИТОГО</h3></td>
            <td></td>
            <td></td>
            <td id="sum"></td>
        </tr>
        </tbody>
    </table>
</div>

<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>