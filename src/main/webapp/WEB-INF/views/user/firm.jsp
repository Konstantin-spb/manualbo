<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: konstantin
  Date: 24.01.18
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../fragments/header.jsp"/>

<body>

<div class="container">
    <form:form method="POST" modelAttribute="firmAndAddress" class="form-horizontal">
    <table class="table table-striped">
        <tbody>
        <tr>
            <td>
                Название фирмы
            </td>
            <td>
                <form:input type="text" path="firmName" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="firmName" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Комментарий для фирмы
            </td>
            <td>
                <form:input type="text" path="commentForFirm" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="commentForFirm" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Индекс
            </td>
            <td>
                <form:input type="text" path="cityIndex" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="cityIndex" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Город
            </td>
            <td>
                <form:input type="text" path="cityName" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="cityName" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Улица
            </td>
            <td>
                <form:input type="text" path="street" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="street" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Номер дома
            </td>
            <td>
                <form:input type="text" path="buildingNumber" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="buildingNumber" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Корпус
            </td>
            <td>
                <form:input type="text" path="buildingKorpus" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="buildingKorpus" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Литера
            </td>
            <td>
                <form:input type="text" path="buildingLitera" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="buildingLitera" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Телефон
            </td>
            <td>
                <form:input type="text" path="telephone" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="telephone" class="help-inline"/>
            </td>
        </tr>
        <tr>
            <td>
                Комментарий для адреса
            </td>
            <td>
                <form:input type="text" path="commentForAddress" class="form-control input-sm"/>
                <div class="has-error">
                        <form:errors path="commentForAddress" class="help-inline"/>

                        <%--<form:input type="hidden" path="boId"/>--%>
            </td>
        </tr>
        <tr>
            <td>
                <form method="post">
                    <button type="submit" class="btn btn-success">СОХРАНИТЬ</button>
                </form>
            </td>
        </tr>
        </form:form>
        </tbody>
    </table>
        ${buildingObjectId}
</div>
<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>
