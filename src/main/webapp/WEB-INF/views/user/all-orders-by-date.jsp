<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%--
  Created by IntelliJ IDEA.
  User: konstantin
  Date: 28.05.18
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <td>
            <h1>Список ордеров за период</h1>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            Фильтр по дате<br>счёта:
        </td>
        <td>
            <form action="<c:url value='/users/${appUser.loginName}/filter-bo-by-date'/>" method="POST">
                <input type="date" name="startDate" value="2010-01-01" min="2000-01-01">
                <input type="date" name="endDate" value="<fmt:formatDate type="time" value="${now}" pattern="yyyy-MM-dd"/>" min="2000-01-01">
                <button type="submit" class="btn btn-secondary">Отфильтровать</button>
            </form>
        </td>
    </tr>
</table>
</body>
</html>
