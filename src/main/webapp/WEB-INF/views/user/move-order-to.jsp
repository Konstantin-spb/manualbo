<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container">

    <table class="table table-striped">
        <thead>
        <th>Номер счета</th>
        <th>Название строительного объекта</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <form action="<c:url value='/users/${loggedinuser}/bo-${oldBoId}/order-${order.id}/moveto'/>"
                       method="POST">
                <td>
                    <small class="text-muted">Счёт номер -</small>
                        ${order.orderName}<br>
                    <small class="text-muted">${order.firm.firmName}</small>
                </td>
                <td>
                    <form:select path="boId" id="boId" name="boId" class="form-control input-sm">
                        <form:option value="-1" label="-- выберите строительный объект"/>
                        <form:options items="${boList}" itemValue="id" itemLabel="objectName"/>
                    </form:select>
                </td>
                <td>
                    <input type="submit" value="Move order" class="btn btn-primary btn-sm"/>
                </td>
            </form>
        </tr>
        </tbody>
    </table>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>

