<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<spring:url value="/" var="urlHome"/>
<spring:url value='/users/${loggedinuser}/bo-${bo.id}/order/new' var="urlAddNewOrder"/>

<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">Перетбург Электро Строй</a>
        </div>
        <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="${urlAddNewOrder}"><font color="red;">Добавить новый ОРДЕР</font></a></li>
            </ul>
        </div>
    </div>
</nav>

<body>
<div class="container">

    <div style="float: right;">
        <form action="<c:url value='/users/${loggedinuser}/bo-${bo.id}/delete'/>"
              method="get">
            <button type="submit" class="btn btn-danger btn-lg">Удалить строительный объект</button>
        </form>
    </div>
    <%--//Вывод каких-то ошибок--%>
    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1>${bo.objectName}</h1>

    <p>
        <input type="button" value="Закрыть все" onclick=$("div[class^='spoiler_body']").hide('normal')>&nbsp;
        <input type="button" value="Открыть все" onclick=$("div[class^='spoiler_body']").show('normal')>
    </p>

    <table class="table table-reflow">
        <c:forEach items="${orders}" var="order">

            <tr>
                <td width="200">
                    <div style="float: left;">
                        <form action="<c:url value='/users/${loggedinuser}/bo-${bo.id}/order-${order.id}/edit'/>"
                              method="get">
                            <button type="submit" class="btn btn-success">edit</button>
                        </form>
                    </div>
                        <%--<form action="<c:url value='/users/${loggedinuser}/bo-${bo.id}/order-${order.id}/moveto'/>"--%>
                        <%--method="get">--%>
                        <%--<button type="submit" data-toggle="button" class="btn btn-warning">move</button>--%>
                        <%--</form>--%>
                    <div style="float: left;">
                        <a class="btn btn-primary" href="<c:url value='/users/${loggedinuser}/bo-${bo.id}/order-${order.id}/moveto'/>"
                           role="button">move</a>
                    </div>

                    <div>
                        <form action="<c:url value='/users/${loggedinuser}/bo-${bo.id}/order-${order.id}/delete'/>"
                              method="get">
                            <button type="submit" class="btn btn-danger">DEL</button>
                        </form>
                    </div>
                </td>
                <td>
                    <a href="" class="spoiler_links">
                        <small class="text-muted">${order.firm.firmName}</small><small class="text-muted">, Счёт номер - </small>${order.orderName}
                        <br>
                        <small class="text-muted">${order.payDate}</small>
                        <br>
                            <%--${order.firm.firmName.addressesById.cityIndex},--%>
                            <%--${order.firm.firmName.addressesById.city},--%>
                            <%--${order.firm.firmName.addressesById.street},--%>
                            <%--${order.firm.firmName.addressesById.buildingNumber},--%>
                            <%--${order.firm.firmName.addressesById.buildingKorpus},--%>
                            <%--${order.firm.firmName.addressesById.buildingLiter},--%>
                            <%--${order.firm.firmName.addressesById.telephone}--%>
                    </a>
                    <div class="spoiler_body">
                        <table class="table table-bordered table-hover">
                            <c:forEach items="${order.orderRowsById}" var="orderRow">
                                <tr>
                                    <td hidden="hidden">${orderRow.id}</td>
                                    <td class="col-xs-9">${orderRow.item}</td>
                                    <td class="col-xs-1">${orderRow.count}</td>
                                    <td class="col-xs-2 price">${orderRow.price}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.spoiler-title').click(function () {
            $(this).parent().children('div.spoiler-content').toggle('fast');
            return false;
        });
    });
</script>
<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>