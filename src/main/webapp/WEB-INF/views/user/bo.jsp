<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>


<spring:url value="/" var="urlHome" />
<spring:url value='/users/${loggedinuser}/new-bo/add' var="urlAddNewBo" />

<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">Петербург Электро</a>
        </div>
        <%--<div id="navbar">--%>
            <%--<ul class="nav navbar-nav navbar-right">--%>
                <%--<li class="active"><a href="${urlAddNewBo}">Добавить новый строительный объект</a></li>--%>
            <%--</ul>--%>
        <%--</div>--%>
    </div>
</nav>

<body>
<div class="container">
    <div class="row">
        <div class="col-xl-9" style="background-color: bisque">
            <%--<%@include file="authheader.jsp" %>--%>

            <div class="well lead" align="center">
                Новый объект<br>
                <a class="text-danger"><h6>Здесь создаём новый строительный объект</h6></a>
            </div>

            <form:form method="POST" modelAttribute="boWeb" class="form-horizontal">
                <%--<form:input type="hidden" path="id" id="id"/>--%>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable" for="objectName">Назнавие объекта</label>
                        <div class="col-md-7">
                            <form:input type="text" path="objectName" id="objectName" class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="objectName" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">Дата заключения договора</label>
                        <div class="col-md-7">
                            <%--<sf:input type="text" path="startDate" id="startDate_id" class="form-control input-sm"/>--%>
                            <sf:input type="date" path="startDate" id="startDate_id" class="form-control input-sm" value="2016-01-01" min="2000-01-01"/>
                            <div class="has-error">
                                <sf:errors path="startDate" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">Дата сдачи объекта</label>
                        <div class="col-md-7">
                            <sf:input type="date" path="EndDate" id="EndDate_id" class="form-control input-sm"/>
                            <div class="has-error">
                                <sf:errors path="EndDate" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <c:choose>
                            <c:when test="${edit}">
                                <input type="submit" value="Update" class="btn btn-primary btn-sm"/> or <a
                                    href="<c:url value='/' />">Cancel</a>
                            </c:when>
                            <c:otherwise>
                                <input type="submit" value="Create new" class="btn btn-primary btn-sm"/> or <a
                                    href="<c:url value='/' />">Cancel</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>