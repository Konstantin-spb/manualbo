<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: konstantin
  Date: 17.01.18
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container">

    <form:form onsubmit="tableValueToJson('mainTableBody')" modelAttribute="orderWeb" method="POST"
               id="jsonSender" class="form-horizontal">
        <table class="table">
            <tr>
                <td>
                        <%--<input type="hidden" name="springJson" value=""/>--%>
                    <form:input type="hidden" path="jsonArray" id="springJson" value="" name="springJson"
                                class="form-control input-sm"/>
                    <form:errors path="jsonArray" class="help-inline"/>

                        <%--<input type="hidden" name="boId" value="${bo.id}"/>--%>
                    <input type="submit" value="Save order" class="btn btn-primary btn-sm"/>
                </td>
                <td>
                    <form:input type="text" path="orderName" placeholder="введите номер ордера"
                                class="form-control input-sm"/>
                    <form:errors path="orderName" class="help-inline"/>

                        <%--<form:input type="date" path="orderDate" class="form-control input-sm"/>--%>
                        <%--<form:errors path="orderDate" class="help-inline"/>--%>

                        <%--<form:input type="date" path="payDate" class="form-control input-sm" placeholder="Дата оплаты"/>--%>
                        <%--<form:errors path="orderDate" class="help-inline"/>--%>

                </td>

                <td colspan="3"><!--Выберите фирму ->-->
                    <c:choose>
                        <c:when test="${order.firm != null}">
                            <form:select path="firmId" id="firmList_id" class="form-control input-sm">
                                <form:option value="${order.firm.id}" label="${order.firm.firmName}"/>
                                <form:options items="${firmList}" itemValue="id" itemLabel="firmName"/>
                            </form:select>
                        </c:when>
                        <c:otherwise>
                            <form:select path="firmId" id="firmList_id" class="form-control input-sm">
                                <form:option value="-1" label="-- choose firm"/>
                                <form:options items="${firmList}" itemValue="id" itemLabel="firmName"/>
                            </form:select>
                        </c:otherwise>
                    </c:choose>
                </td>

                <td>
                    <!--или создайте новую -> -->
                    <form  action="<c:url value='/users/${loggedinuser}/new-firm/create/'/>" method="get">
                        <input type="hidden" value="${bo.id}" name="boId">
                        <button formaction="<c:url value='/users/${loggedinuser}/new-firm/create/'/>" type="submit" class="btn btn-success" formmethod="get">Создать новую фирму</button>
                    </form>
                    <%--<form:form action="<c:url value='/firms/${loggedinuser}/new-firm/create/pls'/>" method="get">--%>
                        <%--<input type="hidden" value="${bo.id}" name="boId">--%>
                        <%--<button type="submit" class="btn btn-success">Создать новую фирму</button>--%>
                    <%--</form:form>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <lable>Дата создания</lable>
                </td>
                <td>
                    <form:input type="date" path="orderDate" class="form-control input-sm"/>
                    <form:errors path="orderDate" class="help-inline"/>
                </td>
            <tr>
                <td>
                    <lable>Дата оплаты</lable>
                </td>
                <td>
                    <form:input type="date" path="payDate" class="form-control input-sm"/>
                    <form:errors path="orderDate" class="help-inline"/>
                </td>
            </tr>
            </tr>
        </table>
    </form:form>


    <table class="table table-striped">
        <thead>
        <tr>
            <th>Действия</th>
            <th>Наименование товара</th>
            <th>кол-во</th>
            <th>Ед.изм.</th>
            <th>Сумма</th>
            <th>Комментарии</th>
        </tr>
        </thead>
        <tbody id="mainTableBody">
        <c:forEach items="${order.orderRowsById}" var="orderRow">
            <script>
                <%--var percentage = "";--%>
                <%--<c:forEach items="${orderRow.orderrowsPercentageList}" var="orp">--%>
                <%--<c:choose>--%>
                <%--<c:when test="${orp.buildingObject.id == bo.id}">--%>
                <%--percentage = ${orp.percentage};--%>
                <%--</c:when>--%>
                <%--<c:otherwise>--%>
                <%--&lt;%&ndash;percentage = ${orp.percentage};&ndash;%&gt;--%>
                <%--</c:otherwise>--%>
                <%--</c:choose>--%>
                <%--</c:forEach>--%>
                addRowToOrder("${orderRow.item}", "${orderRow.count}", "${orderRow.unit}", "${orderRow.price}", "${orderRow.comments}", "${orderRow.id}");
            </script>
        </c:forEach>
        </tbody>
    </table>

    <table class="table table-striped">
        <tbody>
        <th>Наименование</th>
        <th>Кол-во</th>
        <th>Ед.изм.</th>
        <th>Сумма</th>
        <th>Комментарии</th>
        <%--<th>БО</th>--%>
        <form:form method="POST" modelAttribute="orderRowWeb" class="form-horizontal">

            <form:input type="hidden" path="rowId" id="rowId_id" class="form-control input-sm"/>
            <div class="has-error">
                <form:errors path="item" class="help-inline"/>
            </div>

            <tr class="line_tr">
                <td>
                    <form:input type="text" path="item" id="item_id" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="item" class="help-inline"/>
                    </div>
                </td>
                <td>
                    <form:input type="text" path="count" id="count_id" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="count" class="help-inline"/>
                    </div>
                </td>
                <td>
                    <form:input type="text" path="unit" id="unit_id" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="unit" class="help-inline"/>
                    </div>
                </td>
                <td>
                    <form:input type="text" path="price" id="price_id" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="price" class="help-inline"/>
                    </div>
                </td>
                <td>
                    <form:input type="text" path="comments" id="comments_id" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="price" class="help-inline"/>
                    </div>
                </td>

            </tr>
            <tr>
                <td>
                    <a href="javascript://" onclick="addRowToOrder(document.getElementById('item_id').value,
                    document.getElementById('count_id').value,
                    document.getElementById('unit_id').value,
                    document.getElementById('price_id').value,
                    document.getElementById('comments_id').value
                    /*document.getElementById('boId_id').value*/);return false;">Добавить строку</a>
                </td>
            </tr>
        </form:form>
        </tbody>
    </table>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>
