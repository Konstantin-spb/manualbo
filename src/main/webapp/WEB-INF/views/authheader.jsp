<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<sec:authentication var="user" property="principal"/>

<div class="authbar">
    <span>Dear <strong>${loggedinuser}</strong>, Добро пожаловать ${user.username}.</span>
    <span>
        <a href="<c:url value="/logout" />">Logout</a>
    </span>
</div>
<sec:authorize access="hasRole('ADMIN')">
<div>
        <form action="<c:url value='/admin/panel'/>" method="get">
            <button type="submit">Admin panel</button>
        </form>

</div>
<div>

    <span>
        <label>
         Add affiliate programs from file
     </label>
        <form method="POST" action="<c:url value='/users/${myUser.loginName}/add-ap-from-file'/>"
              enctype="multipart/form-data">
            <input type="file" name="file"/><br/>
            <input type="submit" value="Submit"/>
        </form>
    </span>
    </sec:authorize>

    <span>
        <form action="<c:url value='/users/${myUser.loginName}/add-new-affiliate-program-into-your-list'/>"
              method="get">
            <button type="submit">Add affiliate program</button>
        </form>
    </span>
</div>

<div>
    <span>
     <label>
         Add your logins-data from file
     </label>
        <form method="POST" action="<c:url value='/users/${myUser.loginName}/uploadLoginFile'/>"
              enctype="multipart/form-data">
            <input type="file" name="file"/><br/>
            <input type="submit" value="Submit"/>
        </form>
    </span>

    <span>
    <form action="<c:url value='/users/${myUser.loginName}/download-all-stats'/>" method="get">
    <button type="submit">Download all stats</button>
    </form>
    </span>
    <span>
    <form action="<c:url value='/users/${myUser.loginName}/show-all-stats'/>" method="POST">
    <p>Выберите дату:
    <input type="date" name="startDate" value="2016-01-01" min="2000-01-01">
    <input type="date" name="endDate" value="" min="2000-01-01">
    <button type="submit">Show stats by period</button>
    </form>
    </span>
</div>

