package webobject

import java.util.*

data class BuildingObjectProxy(
        var id: Int = -1,
        var name: String = "",
        var startDate: Date = Date(),
        var endDate: Date = Date(),
        var sum: Long = 0L
)