package webobject

data class BoAndOrders(var boId: String = "",
                       var boName: String = "",
                       var orderId: String = "",
                       var orderName: String = "",
                       var item: String = "",
                       var price: String = "")