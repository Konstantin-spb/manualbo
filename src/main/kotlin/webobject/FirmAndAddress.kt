package webobject

data class FirmAndAddress(var firmName:String = "",
                          var commentForFirm:String = "",
                          var cityIndex: String = "",
                          var cityName: String = "",
                          var street: String = "",
                          var buildingNumber: String ="",
                          var buildingKorpus: String = "",
                          var buildingLitera: String = "",
                          var telephone: String = "",
                          var commentForAddress: String = "",
                          var boId: String = "")