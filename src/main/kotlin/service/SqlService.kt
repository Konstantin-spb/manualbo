package service

import net.ew2013.model.BuildingObject
import webobject.BuildingObjectProxy
import java.util.*

fun filterOrderByDateRange(buildingObjects: List<BuildingObject>, startDate: Date, endDate: Date): MutableList<BuildingObjectProxy> {
    for (bo in buildingObjects) {
        val orders = bo.orders.filter { order -> (order.payDate != null && (order.payDate in startDate..endDate)) }
        bo.orders.clear()
        bo.orders.addAll(orders)
//        var summa = 0L
//        orders.forEach { summa += it.orderRowsById.map { it.price }.sum() }

//        for (order in orders) {
//            order.orderRowsById
//        }
        println()
    }
    val boList = buildingObjects.filter { it.orders.size > 0 }
    val webBo = mutableListOf<BuildingObjectProxy>()
    boList.forEach {
        val buildingObjectProxy = BuildingObjectProxy()
        buildingObjectProxy.id = it.id
        buildingObjectProxy.name = it.objectName
        buildingObjectProxy.startDate = it.startDate
//        buildingObjectProxy.endDate = it.endDate

        var summa = 0L
        it.orders.forEach { order -> summa += order.orderRowsById.map { it.price }.sum() }
        buildingObjectProxy.sum = summa
        webBo.add(buildingObjectProxy)
    }
    return webBo
}